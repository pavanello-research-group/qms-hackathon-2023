# QMS Hackathon 2023

This is Michele's talk at the QMS Hackathon that took place on September 12, 2023.

## To run the code, you should install the following packages

 - `pip install dftpy, qepy`

 ## To view the slides in "presentation mode"
 
 - https://rise.readthedocs.io/en/stable/

## Contacts

Michele Pavanello
 - m.pavanello@rutgers.edu
 - @MikPavanello on X
